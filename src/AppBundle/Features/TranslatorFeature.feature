@translator
# language: ru

Функционал: Тестируем переводы

  Предыстория: Вход на сайт и установка русского интерфейса
    Допустим я нахожусь на главной странице
    Когда я перехожу по маршруту: fos_user_security_login
    И я вижу слово "Запомнить меня" где-то на странице
    Когда я заполняю поле формы: _username, значением: user
    Когда я заполняю поле формы: _password, значением: 123
    Когда я нажимаю на кнопку: Войти
    И я вижу слово "user" где-то на странице
    Когда я перехожу по ссылке: translator_change_language со значением: _locale = ru

  Сценарий: Добавление 10 самых популярных поисковых фраз для перевода
    Когда я добавляю фразы:
      | фраза       |
      | купить      |
      | продать     |
      | ремонт      |
      | скачать mp3 |
      | ошибка      |
      | что такое   |
      | как выучить |
      | посоветуйте |
      | фильм       |
      | книга       |
    Тогда я вижу на странице слова:
      | фраза       |
      | купить      |
      | продать     |
      | ремонт      |
      | скачать mp3 |
      | ошибка      |
      | что такое   |
      | как выучить |
      | посоветуйте |
      | фильм       |
      | книга       |

  Сценарий: Добавление 10 самых популярных поисковых фраз для перевода
    Когда я нажимаю на ссылку: купить
    Когда я перевожу слово на языки:
      | язык | перевод |
      | en   | buy     |
      | es   | comprar |
      | fr   | acheter |
      | ja   | 購入      |

    Когда я нажимаю на ссылку: Назад
    Когда я нажимаю на ссылку: продать
    Когда я перевожу слово на языки:
      | язык | перевод |
      | en   | sell    |
      | es   | vender  |
      | fr   | vendre  |
      | ja   | 売る      |

    Когда я нажимаю на ссылку: Назад
    Когда я нажимаю на ссылку: ремонт
    Когда я перевожу слово на языки:
      | язык | перевод      |
      | en   | repairs      |
      | es   | reparaciones |
      | fr   | réparations  |
      | ja   | 修理           |

    Когда я нажимаю на ссылку: Назад
    Когда я нажимаю на ссылку: скачать mp3
    Когда я перевожу слово на языки:
      | язык | перевод         |
      | en   | download mp3    |
      | es   | descargar mp3   |
      | fr   | télécharger mp3 |
      | ja   | mp3をダウンロード      |

    Когда я нажимаю на ссылку: Назад
    Когда я нажимаю на ссылку: ошибка
    Когда я перевожу слово на языки:
      | язык | перевод |
      | en   | error   |
      | es   | error   |
      | fr   | bug     |
      | ja   | バグ      |

    Когда я нажимаю на ссылку: Назад
    Когда я перехожу по ссылке: translator_change_language со значением: _locale = en
    Тогда я вижу на странице слова:
      | фраза        |
      | buy          |
      | sell         |
      | repairs      |
      | download mp3 |
      | error        |
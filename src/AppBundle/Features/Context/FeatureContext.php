<?php

namespace AppBundle\Features\Context;

use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\HttpKernel\KernelInterface;


/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements KernelAwareContext
{
    /** @var  KernelInterface */
    private $kernel;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @When /^я вижу слово "([^"]*)" где\-то на странице$/
     * @param $arg1
     */
    public function яВижуСловоГдеТоНаСтранице($arg1)
    {
        $this->assertPageContainsText($arg1);
    }

    protected function getContainer()
    {
        return $this->kernel->getContainer();
    }

    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function яНахожусьНаГлавнойСтранице()
    {
        $this->visit($this->getContainer()->get('router')->generate('translator_index'));
    }

    /**
     * @When /^я перехожу по маршруту: ([^"]*)$/
     * @param $routeName
     */
    public function яПерехожуПоСсылке($routeName)
    {
        $this->visit($this->getContainer()->get('router')->generate($routeName));
    }

    /**
     * @When /^я перехожу по ссылке: ([^"]*) со значением: ([^"]*) = ([^"]*)$/
     * @param $routeName
     * @param $param
     */
    public function яПерехожуПоСсылкеСоЗначением($routeName, $param, $value)
    {
        $this->visit($this->getContainer()->get('router')->generate($routeName, [$param => $value]));
    }

    /**
     * @When /^я нажимаю на ссылку: ([^"]*)$/
     * @param $link
     */
    public function яНажимаюНаСсылку($link)
    {
        $this->clickLink($link);
    }

    /**
     * @When /^я заполняю поле формы: ([^"]*), значением: ([^"]*)$/
     * @param $field
     * @param $value
     */
    public function яЗаполняюПолеФормыЗначением($field, $value)
    {
        $this->fillField($field, $value);
    }

    /**
     * @When /^я нажимаю на кнопку: ([^"]*)$/
     * @param $button
     */
    public function яНажимаюНаКнопку($button)
    {
        $this->pressButton($button);
    }

    /**
     * @When /^я добавляю фразы:$/
     * @param TableNode $table
     */
    public function яДобавляюФразы(TableNode $table)
    {
        $hash = $table->getHash();
        foreach ($hash as $row) {
            $this->яЗаполняюПолеФормыЗначением('form[phrase]', $row['фраза']);
            $this->яНажимаюНаКнопку('form[save]');
        }
    }

    /**
     * @When /^я вижу на странице слова:$/
     * @param TableNode $table
     */
    public function яВижуНаСтраницеСлова(TableNode $table){
        $hash = $table->getHash();
        foreach ($hash as $row) {
            $this->яВижуСловоГдеТоНаСтранице($row['фраза']);
        }
    }

    /**
     * @When /^я перевожу слово на языки:$/
     * @param TableNode $table
     */
    public function ЯПеревожуСловоНаЯзыки(TableNode $table){
        $hash = $table->getHash();
        foreach ($hash as $row) {
            $this->яЗаполняюПолеФормыЗначением("form[{$row['язык']}]", $row['перевод']);
        }
        $this->яНажимаюНаКнопку('form[save]');
    }

    /**
     * Sets Kernel instance.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }
}
